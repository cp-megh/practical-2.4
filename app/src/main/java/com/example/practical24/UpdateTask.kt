package com.example.practical24

import android.app.AlarmManager
import android.app.DatePickerDialog
import android.app.PendingIntent
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import com.example.practical24.broadcastReceiver.BroadcastManager
import com.example.practical24.constants.Const
import com.example.practical24.database.AppDatabase
import com.example.practical24.database.TasksTable
import com.example.practical24.databinding.ActivityUpdateTaskBinding
import com.example.practical24.model.Tasks
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

class UpdateTask : AppCompatActivity() {
    lateinit var binding: ActivityUpdateTaskBinding
    var appDatabase: AppDatabase? = null
    var date: String? = null
    var time: String? = null

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUpdateTaskBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //Initialize Database
        appDatabase = Room.databaseBuilder(this, AppDatabase::class.java, Const.DB_NAME)
            .allowMainThreadQueries().build()

        //Retrieve task's data from database
        if (intent.getIntExtra(getString(R.string.task_id), 0) >= 0) {
            Companion.taskId = intent.getIntExtra(getString(R.string.task_id), 0)
            val tasksTable: List<TasksTable> =
                appDatabase?.tasksDao()?.getTask(Companion.taskId) as List<TasksTable>
            appDatabase?.close()

            val tasks = Tasks(
                tasksTable[0].id,
                tasksTable[0].title,
                tasksTable[0].description,
                tasksTable[0].date,
                tasksTable[0].time
            )
            binding.etTaskTitle.setText(tasks.title)
            binding.etTaskDescription.setText(tasks.description)
            binding.etTaskDate.setText(tasks.date)
            binding.etTaskTime.setText(tasks.time)
        }

        //New date picker
        binding.etTaskDate.setOnFocusChangeListener { _, focus ->
            if (focus) {
                showDatePicker()
            }
            binding.etTaskDate.clearFocus()
        }

        //New time picker
        binding.etTaskTime.setOnFocusChangeListener { _, focus ->
            if (focus) {
                showTimePicker()
            }
            binding.etTaskTime.clearFocus()
        }

        //Update data in database
        binding.btnUpdateTask.setOnClickListener {
            val title: String =
                Objects.requireNonNull(binding.etTaskTitle.text).toString()
                    .trim { it <= ' ' }
            val description: String =
                Objects.requireNonNull(binding.etTaskDescription.text).toString()
                    .trim { it <= ' ' }
            val date: String =
                Objects.requireNonNull(binding.etTaskDate.text).toString()
                    .trim { it <= ' ' }
            val time: String =
                Objects.requireNonNull(binding.etTaskTime.text).toString()
                    .trim { it <= ' ' }
            //Insert task in database
            if (title == "") {
                binding.etTaskTitle.error = getString(R.string.task_title_error)
            } else if (description == "") {
                binding.etTaskDescription.error = getString(R.string.task_desc_error)
            } else {
                updateTask(title, description, date, time)
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun updateTask(title: String, description: String, date: String, time: String) {
        appDatabase?.tasksDao()?.updateTask(Companion.taskId, title, description, date, time)
        appDatabase?.close()

        createNotification(title, description, date, time, Companion.taskId)
        val intent = Intent(applicationContext, MainActivity::class.java)
        startActivity(intent)
    }

    //Create Notification for updated task
    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotification(
        title: String,
        description: String,
        date: String,
        time: String,
        lastId: Int,
    ) {
        val year: Int = date.substring(6, 10).toInt()
        val month: Int = date.substring(3, 5).toInt() - 1
        val dayOfMonth: Int = date.substring(0, 2).toInt()
        val mHour: Int = time.substring(0, 2).toInt()
        val mMinute: Int = time.substring(3, 5).toInt()
        val cal = Calendar.getInstance()
        cal.timeInMillis = System.currentTimeMillis()
        cal.clear()
        cal[year, month, dayOfMonth, mHour] = mMinute
        val myIntent1 = Intent(applicationContext, BroadcastManager::class.java)
        myIntent1.putExtra(getString(R.string.title_key), title)
        myIntent1.putExtra(getString(R.string.desc_key), description)
        myIntent1.putExtra(getString(R.string.request_code_key), lastId)
        val pendingIntent = PendingIntent.getBroadcast(
            applicationContext, lastId, myIntent1,
            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
        )
        val alarmManager1 = getSystemService(ALARM_SERVICE) as AlarmManager
        val sdkInt = Build.VERSION.SDK_INT
        val versionCodes = Build.VERSION_CODES.O
        if (sdkInt >= versionCodes) {
            alarmManager1.setExactAndAllowWhileIdle(
                AlarmManager.RTC_WAKEUP,
                cal.timeInMillis,
                pendingIntent
            )
        }
    }

    private fun showTimePicker() {
        // Get Current Time
        val c = Calendar.getInstance()
        val mHour = c[Calendar.HOUR_OF_DAY]
        val mMinute = c[Calendar.MINUTE]
        val timePickerDialog = TimePickerDialog(this,
            { _, hourOfDay, minute ->
                val formatter: NumberFormat = DecimalFormat(getString(R.string._00))
                time =
                    formatter.format(hourOfDay.toLong()) + ":" + formatter.format(minute.toLong())
                binding.etTaskTime.setText(time)
            }, mHour, mMinute, false
        )
        timePickerDialog.show()
    }

    private fun showDatePicker() {
        val calendar = Calendar.getInstance()
        val year = calendar[Calendar.YEAR]
        val month = calendar[Calendar.MONTH]
        val dayOfMonth = calendar[Calendar.DAY_OF_MONTH]
        val pickerDialog = DatePickerDialog(this@UpdateTask,
            { _, year1, month1, dayOfMonth1 ->
                val formatter: NumberFormat = DecimalFormat(getString(R.string._00))
                date =
                    formatter.format(dayOfMonth1.toLong()) + "/" + formatter.format((month1 + 1).toLong()) + "/" + year1
                binding.etTaskDate.setText(date)
                binding.layoutTaskDate.isErrorEnabled = false
            }, year, month, dayOfMonth
        )
        pickerDialog.show()
    }

    companion object {
        private var taskId = 0
    }
}