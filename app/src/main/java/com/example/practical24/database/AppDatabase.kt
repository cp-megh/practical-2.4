package com.example.practical24.database

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [TasksTable::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun tasksDao(): TasksDao?
}