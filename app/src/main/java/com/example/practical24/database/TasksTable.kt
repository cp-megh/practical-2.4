package com.example.practical24.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tasks_table")
data class TasksTable(
    @field:ColumnInfo(name = "title") var title: String,
    @field:ColumnInfo(name = "desc") var description: String,
    @field:ColumnInfo(
        name = "date") var date: String,
    @field:ColumnInfo(name = "time") var time: String,
    @field:ColumnInfo(
        name = "favourite") var isFavourite: Boolean,
    @field:ColumnInfo(name = "status") var status: String,
) {
    @PrimaryKey(autoGenerate = true)
    var id = 0

}
