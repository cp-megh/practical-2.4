package com.example.practical24.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface TasksDao {

    @Insert
    fun insertTask(tasks: TasksTable?)


    @Query("delete from tasks_table where id=:taskId")
    fun deleteTask(taskId: Int)

    @Query("update tasks_table set title=:title, 'desc'=:description,date=:date,time=:time where id=:tasksId")
    fun updateTask(tasksId: Int, title: String?, description: String?, date: String?, time: String?)

    @Query("select * from tasks_table where id=:taskId")
    fun getTask(taskId: Int): List<TasksTable>

    @Query("select * from tasks_table")
    fun getAllTasks(): List<TasksTable>

    @Query("SELECT id FROM tasks_table WHERE id = (SELECT MAX(id) FROM tasks_table)")
    fun lastId(): Int


}