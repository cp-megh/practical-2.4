package com.example.practical24.alarmService

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import android.os.IBinder
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.practical24.MainActivity
import com.example.practical24.R


class AlarmService : Service() {
    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        try {
            var requestCode = 0
            intent.let {
                val bundle = intent?.extras
                requestCode = bundle!!.getInt("request_code")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    createNotificationChannel()
                }


                //Create Notification
                val myIntent1 = Intent(applicationContext, MainActivity::class.java)
                val pendingIntent =
                    PendingIntent.getActivity(applicationContext, requestCode, myIntent1,
                        PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT)

                val builder = NotificationCompat.Builder(this, getString(R.string.NEWS_CHANNEL_ID))
                    .setSmallIcon(R.drawable.ic_notification)
                    .setLargeIcon(BitmapFactory.decodeResource(resources, R.drawable.ic_icon_large))
                    .setContentTitle(intent.getStringExtra(getString(R.string.title_key)))
                    .setContentText(intent.getStringExtra(getString(R.string.desc_key)))
                    .addAction(R.drawable.ic_baseline_remove_red_eye_24, "View", pendingIntent)
                    .setStyle(NotificationCompat.BigTextStyle()
                        .bigText(intent.getStringExtra(getString(R.string.desc_key))))
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setChannelId(getString(R.string.NEWS_CHANNEL_ID))
                    .setAutoCancel(true)
                val notificationManagerCompat = NotificationManagerCompat.from(this)
                notificationManagerCompat.notify(resources.getInteger(R.integer.notificationId),
                    builder.build())
            }
        } catch (exception: Exception) {

        }

        return START_STICKY
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel() {
        try {
            val sdkInt = Build.VERSION.SDK_INT
            val versionCodes = Build.VERSION_CODES.O
            if (sdkInt >= versionCodes) {
                //Toast.makeText(getApplicationContext(), ""+sdk_int+"/"+version_codes, Toast.LENGTH_SHORT).show();
                val notificationChannel = NotificationChannel(getString(R.string.NEWS_CHANNEL_ID),
                    getString(R.string.CHANNEL_NEWS),
                    NotificationManager.IMPORTANCE_HIGH)
                notificationChannel.description = getString(R.string.CHANNEL_DESCRIPTION)
                notificationChannel.setShowBadge(true)
                val notificationManager = getSystemService(
                    NotificationManager::class.java)
                notificationManager.createNotificationChannel(notificationChannel)
            }
        } catch (exception: Exception) {
            Toast.makeText(applicationContext, "Exception occurred!$exception", Toast.LENGTH_SHORT)
                .show()
        }
    }

}
