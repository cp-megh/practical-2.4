package com.example.practical24.broadcastReceiver

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.example.practical24.alarmService.AlarmService

class BroadcastManager : BroadcastReceiver() {
    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    override fun onReceive(context: Context, intent: Intent) {
        try {
            val bundle = intent.extras
            val title = bundle!!.getString("title")
            val description = bundle.getString("description")
            val requestCode = bundle.getString("request_code")
            val i = Intent(context.applicationContext, AlarmService::class.java)
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            i.putExtra("title", title)
            i.putExtra("description", description)
            i.putExtra("request_code", requestCode)
            context.startService(i)
        } catch (exception: Exception) {
            Toast.makeText(context, "Exception occurred!", Toast.LENGTH_SHORT).show()
        }
    }
}