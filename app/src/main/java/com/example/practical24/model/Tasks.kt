package com.example.practical24.model

data class Tasks(
    var id: Int,
    var title: String,
    var description: String,
    var date: String,
    var time: String,
)
