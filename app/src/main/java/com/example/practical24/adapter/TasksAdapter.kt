package com.example.practical24.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.practical24.databinding.CustomTasksViewBinding
import com.example.practical24.model.Tasks
import java.util.*

class TasksAdapter() : RecyclerView.Adapter<TasksAdapter.ViewHolder>() {

    var activity: Activity? = null
    var tasksArrayList: ArrayList<Tasks>? = null
    var customBinding: CustomTasksViewBinding? = null
    private var onItemClickListener: OnItemClickListener? = null


    interface OnItemClickListener {
        fun onItemClicked(id: Int, pos: Int, taskId: Int)
    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener) {
        this.onItemClickListener = onItemClickListener
    }

    constructor(activity: Activity, tasksArrayList: ArrayList<Tasks>?) : this() {
        this.activity = activity
        this.tasksArrayList = tasksArrayList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        customBinding = CustomTasksViewBinding.inflate(LayoutInflater.from(activity), parent, false)
        return ViewHolder(customBinding!!)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val tasks: Tasks = tasksArrayList!![position]
        holder.customBinding.txtTitle.text = tasks.title
        holder.customBinding.txtDescription.text = tasks.description
        if (!tasks.date.equals("")) {
            holder.customBinding.chipDate.text = tasks.date
        } else {
            holder.customBinding.chipDate.visibility = View.INVISIBLE
        }
        if (!tasks.time.equals("")) {
            holder.customBinding.chipTime.text = tasks.time
        } else {
            holder.customBinding.chipTime.visibility = View.INVISIBLE
        }
        holder.customBinding.ivDelete.setOnClickListener {
            val id: Int = tasks.id
            onItemClickListener?.onItemClicked(holder.customBinding.ivDelete.id, position, id)
        }
        holder.customBinding.taskLayout.setOnClickListener {
            val id: Int = tasks.id
            onItemClickListener?.onItemClicked(holder.customBinding.taskLayout.id, position, id)
        }
    }

    override fun getItemCount(): Int {
        return tasksArrayList!!.size
    }


    class ViewHolder(var customBinding: CustomTasksViewBinding) :
        RecyclerView.ViewHolder(customBinding.root)
}
