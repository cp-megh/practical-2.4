package com.example.practical24

import android.app.*
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import com.example.practical24.broadcastReceiver.BroadcastManager
import com.example.practical24.constants.Const
import com.example.practical24.database.AppDatabase
import com.example.practical24.database.TasksTable
import com.example.practical24.databinding.ActivityAddTaskBinding
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

class AddTask : AppCompatActivity() {
    lateinit var binding: ActivityAddTaskBinding
    var appDatabase: AppDatabase? = null
    var date: String? = null
    var time: String? = null

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddTaskBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //Database Initialization
        appDatabase = Room.databaseBuilder(this, AppDatabase::class.java, Const.DB_NAME)
            .allowMainThreadQueries().build()

        //Select date dialog
        binding.etTaskDate.setOnFocusChangeListener { _, focus ->
            if (focus) {
                showDatePicker()
            }
            binding.etTaskDate.clearFocus()
        }

        //Select time dialog
        binding.etTaskTime.setOnFocusChangeListener { _, focus ->
            if (focus) {
                showTimePicker()
            }
            binding.etTaskTime.clearFocus()
        }

        //Add/insert task into database
        binding.btnCreateTask.setOnClickListener {
            val title: String =
                Objects.requireNonNull(binding.etTaskTitle.text).toString()
                    .trim { it <= ' ' }
            val description: String =
                Objects.requireNonNull(binding.etTaskDescription.text).toString()
                    .trim { it <= ' ' }
            val date: String =
                Objects.requireNonNull(binding.etTaskDate.text).toString()
                    .trim { it <= ' ' }
            val time: String =
                Objects.requireNonNull(binding.etTaskTime.text).toString()
                    .trim { it <= ' ' }
            if (title == "") {
                binding.etTaskTitle.error = getString(R.string.task_title_error)
            } else if (description == "") {
                binding.etTaskDescription.error = getString(R.string.task_desc_error)
            } else {
                addTask(title, description, date, time)
            }
        }
    }

    private fun showTimePicker() {
        // Get Current Time
        val c = Calendar.getInstance()
        val mHour = c[Calendar.HOUR_OF_DAY]
        val mMinute = c[Calendar.MINUTE]
        val timePickerDialog = TimePickerDialog(this,
            { _, hourOfDay, minute ->
                val formatter: NumberFormat = DecimalFormat(getString(R.string._00))
                time =
                    formatter.format(hourOfDay.toLong()) + ":" + formatter.format(minute.toLong())
                binding.etTaskTime.setText(time)
            }, mHour, mMinute, false
        )
        timePickerDialog.show()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun addTask(title: String, description: String, date: String, time: String) {
        val tasks = TasksTable(title, description, date, time, false, Const.PENDING_STATUS)
        appDatabase?.tasksDao()?.insertTask(tasks)
        appDatabase?.close()

        val lastId: Int = appDatabase?.tasksDao()!!.lastId()
        appDatabase?.close()

        createNotification(title, description, date, time, lastId)
        val intent = Intent(applicationContext, MainActivity::class.java)
        startActivity(intent)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun createNotification(
        title: String?,
        description: String?,
        date: String,
        time: String,
        lastId: Int,
    ) {
        val year: Int = date.substring(6, 10).toInt()
        val month: Int = date.substring(3, 5).toInt() - 1
        val dayOfMonth: Int = date.substring(0, 2).toInt()
        val mHour: Int = time.substring(0, 2).toInt()
        val mMinute: Int = time.substring(3, 5).toInt()
        val cal = Calendar.getInstance()
        cal.timeInMillis = System.currentTimeMillis()
        cal.clear()
        cal[year, month, dayOfMonth, mHour] = mMinute
        val myIntent1 = Intent(applicationContext, BroadcastManager::class.java)
        myIntent1.putExtra(getString(R.string.title_key), title)
        myIntent1.putExtra(getString(R.string.desc_key), description)
        myIntent1.putExtra(getString(R.string.request_code_key), lastId)
        val pendingIntent = PendingIntent.getBroadcast(
            applicationContext, lastId, myIntent1,
            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
        )
        val alarmManager1 = getSystemService(ALARM_SERVICE) as AlarmManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            alarmManager1.setExactAndAllowWhileIdle(
                AlarmManager.RTC_WAKEUP,
                cal.timeInMillis,
                pendingIntent
            )
        }
    }

    override fun onBackPressed() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage(R.string.cancel_task_add)
            .setCancelable(true)
            .setPositiveButton(
                getString(R.string.positive_message)
            ) { _, _ ->
                val intent = Intent(applicationContext, MainActivity::class.java)
                startActivity(intent)
            }
            .setNegativeButton(
                getString(R.string.negative_message)
            ) { _, _ -> }
        val alertDialog = builder.create()
        alertDialog.setTitle(getString(R.string.cancel_task_title))
        alertDialog.show()
    }

    private fun showDatePicker() {
        val calendar = Calendar.getInstance()
        val year = calendar[Calendar.YEAR]
        val month = calendar[Calendar.MONTH]
        val dayOfMonth = calendar[Calendar.DAY_OF_MONTH]
        val pickerDialog = DatePickerDialog(this@AddTask,
            { _, year1, month1, dayOfMonth1 ->
                val formatter: NumberFormat = DecimalFormat(getString(R.string._00))
                date =
                    formatter.format(dayOfMonth1.toLong()) + "/" + formatter.format((month1 + 1).toLong()) + "/" + year1
                binding.etTaskDate.setText(date)
                binding.layoutTaskDate.error = null
            }, year, month, dayOfMonth
        )
        pickerDialog.show()
    }
}