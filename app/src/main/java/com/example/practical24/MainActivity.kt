package com.example.practical24

import android.annotation.SuppressLint
import android.app.AlarmManager
import android.app.AlertDialog
import android.app.PendingIntent
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationManagerCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.room.Room
import com.example.practical24.adapter.TasksAdapter
import com.example.practical24.adapter.TasksAdapter.OnItemClickListener
import com.example.practical24.broadcastReceiver.BroadcastManager
import com.example.practical24.constants.Const
import com.example.practical24.database.AppDatabase
import com.example.practical24.database.TasksTable
import com.example.practical24.databinding.ActivityMainBinding
import com.example.practical24.databinding.CustomTasksViewBinding
import com.example.practical24.model.Tasks
import java.util.*

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    var tasksTable: List<TasksTable>? = null
    var tasksArrayList: ArrayList<Tasks>? = null

    var tasksAdapter: TasksAdapter? = null
    var customBinding: CustomTasksViewBinding? = null
    var appDatabase: AppDatabase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.getRoot())
        try {

            //Clear notifications
            val notificationManagerCompat = NotificationManagerCompat.from(this)
            notificationManagerCompat.cancel(resources.getInteger(R.integer.notificationId))

            //User notification according to availability of data
            binding.tasksRecyclerView.visibility = View.VISIBLE
            binding.imageAddTask.visibility = View.GONE
            binding.textAddTask.visibility = View.GONE

            //Initialize database
            appDatabase =
                Room.databaseBuilder(applicationContext, AppDatabase::class.java, Const.DB_NAME)
                    .allowMainThreadQueries().build()

            //Initialize tasks in recycler view
            prepareTasks()

            //Initialization if tasks are found in database
            if (tasksArrayList!!.size > 0) {
                customBinding = CustomTasksViewBinding.inflate(layoutInflater)
                tasksAdapter = TasksAdapter(this@MainActivity, tasksArrayList)
                val layoutManager = GridLayoutManager(this, 2)
                binding.tasksRecyclerView.layoutManager = layoutManager
                binding.tasksRecyclerView.setHasFixedSize(true)
                binding.tasksRecyclerView.adapter = tasksAdapter
                tasksAdapter?.setOnItemClickListener(onItemClickListener = object :
                    OnItemClickListener {
                    @RequiresApi(Build.VERSION_CODES.O)
                    @SuppressLint("NotifyDataSetChanged")
                    override fun onItemClicked(id: Int, pos: Int, taskId: Int) {
                        when (id) {
                            R.id.iv_delete -> {
                                //Toast.makeText(applicationContext, "Id:"+id+"\nPos:"+pos, Toast.LENGTH_SHORT).show()
                                val message =
                                    getString(R.string.remove_message_1) + tasksArrayList!![pos].title + getString(
                                        R.string.remove_message_2)
                                val builder = AlertDialog.Builder(this@MainActivity)
                                builder.setTitle(R.string.delete_alert_title)
                                builder.setMessage(message)
                                builder.setPositiveButton(
                                    R.string.positive_message
                                ) { _, _ ->
                                    tasksArrayList?.removeAt(pos)
                                    appDatabase?.tasksDao()?.deleteTask(taskId)
                                    appDatabase?.close()
                                    tasksAdapter?.notifyDataSetChanged()
                                    val myIntent1 =
                                        Intent(applicationContext, BroadcastManager::class.java)
                                    val pendingIntent = PendingIntent.getBroadcast(
                                        applicationContext, taskId, myIntent1,
                                        PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT)
                                    val alarmManager1 =
                                        getSystemService(ALARM_SERVICE) as AlarmManager
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                        alarmManager1.cancel(pendingIntent)
                                    }
                                }
                                builder.setNegativeButton(
                                    R.string.negative_message
                                ) { _, _ ->
                                    val intent =
                                        Intent(applicationContext, MainActivity::class.java)
                                    startActivity(intent)
                                }
                                val alertDialog: AlertDialog = builder.create()
                                alertDialog.setCancelable(false)
                                alertDialog.show()
                            }
                            R.id.task_layout -> {
                                val intent = Intent(applicationContext, UpdateTask::class.java)
                                intent.putExtra(
                                    getString(R.string.task_id),
                                    tasksArrayList!![pos].id
                                )
                                startActivity(intent)
                            }
                            else -> {}
                        }
                    }
                })
            } else {
                binding.tasksRecyclerView.visibility = View.GONE
                binding.imageAddTask.visibility = View.VISIBLE
                binding.textAddTask.visibility = View.VISIBLE
                binding.imageAddTask.setOnClickListener {
                    val intent = Intent(applicationContext, AddTask::class.java)
                    startActivity(intent)
                }
                binding.textAddTask.setOnClickListener {
                    val intent = Intent(applicationContext, AddTask::class.java)
                    startActivity(intent)
                }
            }

            //Add new task floating button
            binding.floatingActionButton.setOnClickListener {
                val intent = Intent(applicationContext, AddTask::class.java)
                startActivity(intent)
            }
        } catch (exception: Exception) {
            Toast.makeText(
                applicationContext,
                "Exception occurred!$exception",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun prepareTasks() {
        try {
            tasksTable = appDatabase?.tasksDao()?.getAllTasks() as List<TasksTable>?
            appDatabase?.close()
            tasksArrayList = ArrayList<Tasks>()
            for (tasks in tasksTable!!) {
                tasksArrayList?.add(
                    Tasks(
                        tasks.id,
                        tasks.title,
                        tasks.description,
                        tasks.date,
                        tasks.time
                    )
                )
            }
        } catch (exception: Exception) {
            Toast.makeText(applicationContext, "Exception occurred!", Toast.LENGTH_SHORT).show()
        }
    }

}

