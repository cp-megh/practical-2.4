package com.example.practical24.constants

class Const {
    companion object {
        const val DB_NAME = "my_database.db"
        const val PENDING_STATUS = "pending"
    }

}
